<?php include 'include/header.php' ?>

<!-- Space -->
<div class="inner-page-header-seprator"></div>
<!-- Space -->


<section class="alumni-office-news-area">
    <div class="container">
        <h1 class="sec-heading text-center">
            <span>
                Alumni 
            </span>
            Office
        </h1>
        <div class="row">
            <div class="col-lg-4">
                <div class="alumni-office-news">
                    <a href="img/news/1.png" data-fancybox="galler">
                        <img src="img/news/1.png" class="img-fluid" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="alumni-office-news">
                    <a href="img/news/2.png" data-fancybox="galler">
                        <img src="img/news/2.png" class="img-fluid" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="alumni-office-news">
                    <a href="img/news/3.png" data-fancybox="galler">
                        <img src="img/news/3.png" class="img-fluid" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>



<?php include 'include/footer.php' ?>