<!DOCTYPE html>
<html lang="en" data-scroll-container>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Alumni - Habib University</title>
    <meta name="description" content="The first dedicated Liberal Arts &amp; Sciences university that offers interdisciplinary education with its unique Liberal Core." />
    <meta property="og:image" content="https://habib.edu.pk/alumni/img/fb-thumb.jpg" />
	   <meta property="og:image:width" content="1920px" />
       <meta property="og:image:height" content="1080px" />
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png"> <meta property="og:image" content="https://habib.edu.pk/convocation/img/convocation-thumb.jpg" />

     <!-- Pattern To Link Css for Gt Matrix -->
        <!-- Eliminate render-blocking resources  -->
        <!-- <link rel="preload" href="css/style.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
        <noscript><link rel="stylesheet" href="css/style.min.css"></noscript> -->
        <link rel="stylesheet" href="css/style.min.css">
        <link rel="stylesheet" href="css/plug-in.min.css">
        <link rel="stylesheet" href="css/plugin.css">
        <!-- Eliminate render-blocking resources  -->
    <!-- Pattern To Link Css for Gt Matrix -->
</head>
<body>
<!-- Header -->
 <header class="bg-header">
     <div class="container-fluid">
         <nav class="navbar navbar-expand-lg">
              <div class="container-fluid">
                <a class="navbar-brand" href="index">
                    <img src="img/logo.svg" width="" height="" alt="">
                </a>
                <button class="navbar-toggler ms-auto" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <div id="nav-icon3" class="">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </button>
                
                      
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                      <ul class="navbar-nav m-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                          <a class="nav-link active" aria-current="page" href="index">Home</a>
                        </li>
                        <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle" target="_blank" href="https://habibuniversity.sharepoint.com/sites/alumni/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Alumni Portal
                          </a>
                          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="alumni-career-update">Alumni Career Update</a></li>
                            <li><a class="dropdown-item" href="https://habibuniversity.sharepoint.com/sites/Alumni" target="_blank">Policies</a></li>
                            <li><a class="dropdown-item" href="https://habibuniversity.sharepoint.com/sites/alumni/Pages/jobs.aspx" target="_blank">Alumni Jobs</a></li>
                          </ul>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="alumni-affairs">Office of Alumni Affairs</a>
                        </li>
                        <li class="nav-item logo-nav-item">
                            <a class="nav-link logo-nav-link" target="_blank" href="http://habib.edu.pk/">
                              <img src="img/logo.svg" width="" height="" alt="">
                            </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="services">Services</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="volunteering">Volunteering</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="https://habib.edu.pk/career-services/" target="_blank">Careers</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#Contact">Contact us</a>
                        </li>
                      </ul>
                    </div>
              </div>
         </nav>
     </div>
 </header>   
<!-- Header -->

<!-- Banner Area -->
<section class="banner-area">
    <div class="container-fluid px-0">
        <div class="banner-content">
            <!-- <img src="img/main-banner.jpg" class="img-fluid" alt=""> -->
            <h1 class="banner-heading">
                Alumni - Habib University
            </h1>
        </div>
    </div>
</section>
<!-- Banner Area -->

<!-- We Proud Area -->
<section class="we-proud-area">
    <div class="container">
        <div class="row align-items-center">      
            <div class="col-lg-7">
                <img src="img/hu-icon-on-hover.svg" width="" height="" alt="" class="bg-proud-hu-icon">
                <div class="produd-img">
                    <img src="img/proud-1.jpg" width="" height="" alt="">
                    <img src="img/proud-2.jpg" width="" height="" alt="">
                    <img src="img/proud-3.jpg" width="" height="" alt="">
                    <img src="img/proud-4.jpg" width="" height="" alt="">
                </div>
            </div>
            <div class="col-lg-5">
                <div class="proud-content">
                    <h1 class="sec-heading">
                        <span>
                            We Are
                        </span>
                        Proud!
                    </h1>
                    <p>The Habib University alumni community is a network of hundreds of Habib graduates living in Pakistan and around the world. Students automatically become a member of the HU Alumni Association upon their graduation. Our mission is to create a vibrant, engaged and committed global alumni community that both supports the continuing professional success of our alumni and enthusiastically advances the mission of Habib University in multiple ways. The Office of Alumni Affairs strives to build lifelong partnerships and connections between Habib University and its growing alumni community through engaging and inspiring activities and programs that support our mission. Please continue to give us your feedback and let us know about your own progress. Keep in touch!</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- We Proud Area -->

<section class="alumuni-affairs">
    <div class="container">
        <div class="section-cont">
            <!-- <h1 class="sec-heading"> <span>Ipsum</span> Lorem </h1> -->
            <p>Alumni Affairs strives to build lifelong partnerships and connections between Habib University and its growing alumni community through engaging and inspiring activities and programs that support our mission. Please continue to give us your feedback and let us know about your own progress. Keep in touch!</p>
        </div>
        <div class="row events-box-slider">
            <div class="col-lg-3">
                <div class="events-box">
                    <a href="news">
                        <img src="img/Upcoming-Events.jpg" width="" height="" class="img-fluid" alt="Upcoming Events">
                        <h5>Upcoming Events</h5>
                    </a>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="events-box">
                    <a href="https://habib.edu.pk/HU-news/category/alumni/" target="_blank">
                        <img src="img/Archives.jpg" width="" height="" class="img-fluid" alt="Archives">
                        <h5>Archives</h5>
                    </a>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="events-box">
                    <a href="https://www.facebook.com/groups/habibuniversityalumni" target="_blank">
                        <img src="img/Alumni-Facebook.jpg" width="" height="" class="img-fluid" alt="Alumni Facebook">
                        <h5>Alumni Facebook</h5>
                    </a>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="events-box">
                    <a target="_blank" href="https://dukaan.habib.edu.pk/">
                        <img src="img/Discounts.jpg" width="" height="" class="img-fluid" alt="Discounts">
                        <h5>Discounts</h5>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="featured-alumni">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="feature-alumni-cont">
                    <h1 class="sec-heading"> <span>Featured </span>Alumni </h1>
                    <p>
                        <b class="sub-para">Mushba Said</b>
                        Mushba is a writer, artist, and designer who graduated from Habib University in 2019; she majored in Communication & Design and double-minored in Comparative Literature and Development & Policy. Her art-book, Said’s Dictionary, was a class project that utilized the dictionary format to explore themes of mental health and was featured at Focal Point (Sharjah) by Vasl Artist’s Association, and THP Daak (Jaipur, Ladakh, Lahore) by The Happieee Place. Her graphic designs have been featured in Dawn.com and she has also typeset multiple books. Presently, she serves as the Digital Marketing Associate at Active Capital IT and in her spare time she enjoys gardening, reading sci fi and fantasy, and playing video games. To see what else she’s up to, find her on Instagram (@Mushbagram) or Twitter (@HeyMushba)
                    </p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="alumni-profile">
                    <img src="img/Mushba-Said.png" width="" height="" alt="Mushba Said">
                    <p> <i> My favorite place was the pool. Not only did it help me with my health during finals weeks, but helping others to learn swimming helped me build new relationships while also discovering that I have the potential to teach, which was something I previously dismissed about myself.</i></p>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="alumni-services">
    <div class="container">
        <h1 class="sec-heading"> <span>Alumni</span> Services </h1>
        <p class="mb-2"><b>Whether you’re a recent graduate, or it’s been a long time since you studied with us, we’re <br>  here to help keep you involved with the University and your alumni network.</b> </p>
        <p>No matter where life takes you, we want you to feel part of our community forever and we hope you continue <br> to share the spirit long after your graduation day.</p>
        <div class="alumni-box-row events-box-slider">
            <div class="alumni-box">
                <a href="services#HU-Alumni-Card">
                    <img src="img/HU-Alumni-card.jpg" width="" height="" alt="HU Alumni Card">
                    <h5>HU Alumni Card</h5>
                </a>
            </div>
            <div class="alumni-box">
                <a href="services#Access-to-Campus">
                    <img src="img/Access-to-campus.jpg" width="" height="" alt="Access to Campus">
                    <h5>Access to Campus</h5>
                </a>
            </div>
            <div class="alumni-box">
                <a href="services#IT-Servcies">
                    <img src="img/IT-Services.jpg" width="" height="" alt="IT Servcies">
                    <h5>IT Servcies</h5>
                </a>
            </div>
            <div class="alumni-box">
                <a href="services#Library">
                    <img src="img/Library.jpg" width="" height="" alt="Library">
                    <h5>Library</h5>
                </a>
            </div>
            <div class="alumni-box">
                <a href="services#Gym-and-Pool-Facilitiess">
                    <img src="img/Gym-&-pool-facilities.jpg" width="" height="" alt="Gym and Pool Facilities">
                    <h5>Gym and Pool Facilities</h5>
                </a>
            </div>
            <div class="alumni-box">
                <a href="services#Sports-Facilities">
                    <img src="img/Sports-facilities.jpg" width="" height="" alt="Sports Facilities">
                    <h5>Sports Facilities</h5>
                </a>
            </div>
        </div>
        <a href="services" class="view-details">View Details</a>
    </div>
</section>


<section class="access-to-portal">
    <div class="container">
        <div class="row justify-content-end align-items-center">
            <div class="col-xl-4 col-md-12 col-sm-12">
                <div class="access-portal-content">
                    <h1 class="sec-heading"> <span>Access To</span> Alumni Portal</h1>
                    <p> The Office of Alumni Affairs strives to build lifelong partnerships and connections between Habib University and its growing alumni community through engaging and inspiring activities and programs that support our mission. Please continue to give us your feedback and let us know about your own progress. Keep in touch! </p>
                    <div class="alumni-link">
                        <a target="_blank" href="https://habibuniversity.sharepoint.com/sites/Alumni" class="alum-port">
                            <span class="alum-atta">
                                <img src="img/link-icon.svg" width="" height="" alt="">
                                <span class="att-line"></span>
                                Alumni Portal
                            </span>
                            <span class="arrow-click">
                                <img src="img/arrow.svg" width="" height="" alt="">
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="alumni-association">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5">
                <div class="alumn-associa-img">
                    <img src="img/baidge-icon.svg" width="" height="" class="wheel-one" data-aos="rotate-positive-dir" data-aos-duration="2000" alt="">
                    <img src="img/purple-icon.svg" width="" height="" class="wheel-sec" data-aos="rotate-negative-dir" data-aos-duration="2000" alt="">
                </div>
            </div>
            <div class="col-lg-7">
                <div class="alumni-ass-ciat-content">
                    <h1 class="sec-heading"> <span>Alumni </span> Association</h1>
                    <p> The first Alumni Association of the Habib University (HUAA) will be formed in March 2021. The HUAA will comprise HU Alumni (men and women) from all batches (Class of 2018, 2019 and 2020) so as to represent all the graduated batches. The HUAA will consist of the following positions:</p>
                    <ul class="alumni-members">
                        <li>President</li>
                        <li>Member-Policy</li>
                        <li>Member-Projects & Programs</li>
                        <li>Vice President</li>
                        <li>Member-Events</li>
                        <li>Member-Career Development</li>
                        <li>Treasurer</li>
                        <li>Member-Fundraising</li>
                        <li class="li-none"></li>
                        <li>Gen.Secretary</li>
                        <li>Member-Career Development</li>
                        <li class="li-none"></li>
                    </ul>
                    
                </div>
            </div>
        </div>
    </div>
</section>

<section class="featured-news">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-xl-4">
                <div class="featured-news-img">
                    <h2>Featured</h2>
                    <img src="img/featured-news-banner.jpg" width="" height="" class="img-fluid" alt="">
                    <h1 class="news-head">News</h1>
                </div>
            </div>
            <div class="col-xl-8">
                <div class="featured-content">
                    <h1 class="sec-heading"> <span>Featured</span> News</h1>
                    <ul class="featured-box-area">
                        <li>
                            <a href="news-alumni-affairs">
                                <img src="img/Newsletter.jpg" width="" height="" alt="Newsletter">
                                <h5>Newsletter</h5>
                            </a>
                        </li>
                        <li>
                            <a href="https://habib.edu.pk/HU-news/category/alumni/" target="_blank">
                                <img src="img/Alumni-News.jpg" width="" height="" alt="Alumni News">
                                <h5>Alumni News</h5>
                            </a>
                        </li>
                        <li>
                            <a href="https://habib.edu.pk/HU-news/category/alumni/" target="_blank">
                                <img src="img/Campus-News.jpg" width="" height="" alt="Campus News">
                                <h5>Campus News</h5>
                            </a>
                        </li>
                        <li>
                            <a href="https://habib.edu.pk/HU-news/" target="_blank">
                                <img src="img/Career-News.jpg" width="" height="" alt="Career News">
                                <h5>Career News</h5>
                            </a>
                        </li>
                    </ul>
                    
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'include/footer.php' ?>