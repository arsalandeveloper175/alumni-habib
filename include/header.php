<!DOCTYPE html>
<html lang="en" data-scroll-container>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Alumni - Habib University</title>
    <meta name="description" content="The first dedicated Liberal Arts &amp; Sciences university that offers interdisciplinary education with its unique Liberal Core." />
    <meta property="og:image" content="https://habib.edu.pk/alumni/img/fb-thumb.jpg" />
	   <meta property="og:image:width" content="1920px" />
       <meta property="og:image:height" content="1080px" />
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
    <!-- <link rel="stylesheet" href="css/style.min.css"> -->
    <link rel="preload" href="css/style.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="css/style.min.css"></noscript>
    <link rel="stylesheet" href="css/plug-in.min.css">
    <link rel="stylesheet" href="css/plugin.css">
  
        <!-- Pattern To Link Css for Gt Matrix -->
    <!-- <link rel="preload" href="css/plug-in.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="css/plug-in.min.css"></noscript>
    <link rel="preload" href="css/plugin.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="css/plugin.css"></noscript> -->
    <!-- Pattern To Link Css for Gt Matrix -->

        
</head>
<body>
<!-- Header -->
 <header>
     <div class="container-fluid">
         <nav class="navbar navbar-expand-lg">
              <div class="container-fluid">
                <a class="navbar-brand" href="index">
                    <img src="img/logo.svg" alt="">
                </a>
                <button class="navbar-toggler ms-auto" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <div id="nav-icon3" class="">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </button>
                
                      
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                      <ul class="navbar-nav m-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                          <a class="nav-link active" aria-current="page" href="index">Home</a>
                        </li>
                        <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle" target="_blank" href="https://habibuniversity.sharepoint.com/sites/alumni/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Alumni Portal
                          </a>
                          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="alumni-career-update">Alumni Career Update</a></li>
                            <li><a class="dropdown-item" href="https://habibuniversity.sharepoint.com/sites/Alumni" target="_blank">Policies</a></li>
                            <li><a class="dropdown-item" href="https://habibuniversity.sharepoint.com/sites/alumni/Pages/jobs.aspx" target="_blank">Alumni Jobs</a></li>
                          </ul>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="alumni-affairs">Office of Alumni Affairs</a>
                        </li>
                        <li class="nav-item logo-nav-item">
                            <a class="nav-link logo-nav-link" target="_blank" href="http://habib.edu.pk/">
                              <img src="img/logo.svg" alt="">
                            </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="services">Services</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="volunteering">Volunteering</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="https://habib.edu.pk/career-services/" target="_blank">Careers</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#Contact">Contact us</a>
                        </li>
                      </ul>
                    </div>
              </div>
         </nav>
     </div>
 </header>   
<!-- Header -->