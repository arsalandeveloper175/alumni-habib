




    var currentUrl = window.location.href;
    $(window).on('load', function(){
        if (currentUrl == 'https://habib.edu.pk/alumni/services#HU-Alumni-Card'){
                $('.service-slider').trigger('to.owl.carousel', 0)
        }
        if (currentUrl == 'https://habib.edu.pk/alumni/services#Access-to-Campus'){
                $('.service-slider').trigger('to.owl.carousel', 1)
        }
        if (currentUrl == 'https://habib.edu.pk/alumni/services#IT-Servcies'){
            $('.service-slider').trigger('to.owl.carousel', 2)
        }
        if (currentUrl == 'https://habib.edu.pk/alumni/services#Library'){
            $('.service-slider').trigger('to.owl.carousel', 3)
        }
        if (currentUrl == 'https://habib.edu.pk/alumni/services#Gym-and-Pool-Facilitiess'){
            $('.service-slider').trigger('to.owl.carousel', 4)
        }
        if (currentUrl == 'https://habib.edu.pk/alumni/services#Sports-Facilities'){
            $('.service-slider').trigger('to.owl.carousel', 5)
        }
    });

   
    // $('button.owl-next.disabled').click(function(){
    //     console.log('working fine');
    // });




// Alumni - Services
$('.service-slider').owlCarousel({
    loop:false,
    nav: true,
    navText: ['<img src="img/left-icon.svg" alt="">', '<img src="img/right-icon.svg" alt="">'],
    dots: false,
    margin:50,
    autoplay:false,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    mouseDrag: true,
    responsiveClass:true,
    rewindNav:false,
    rewind:false,
    responsive:{
        0:{
            items:6,
            margin:0,
            stagePadding: 0,
        },                 
        992:{
            items:1,
            stagePadding: 32,
        },
        1198: {
            items: 1,  
            stagePadding: 140,
        }, 
        1279: {
            items: 1,
            stagePadding: 140,
        }, 
        1365: {
            items: 1,
            stagePadding: 150,
        },
        1439: {
            items: 1,
            stagePadding: 185,
        },
        1919: {
            items: 1,
            stagePadding: 350,
        }
    }
});
// Alumni - Services




// Project Slider


$('.projects-area-slider').owlCarousel({
    loop:false,
    nav: true,
    navText: ['<img src="img/left-icon.svg" alt="">', '<img src="img/right-icon.svg" alt="">'],
    dots: true,
    margin:25,
    autoplay:false,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    mouseDrag: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            dots: true,
            nav: false,
        },                   
        574:{
            items:2,
            dots: true,
            nav: false,
        },
        766: {
            items: 2,  
            dots: true,
            nav: false,
        }, 
        990: {
            items: 3,
            dots: true,
            nav: true,
        }, 
        1199: {
            items: 4,
            dots: true,
            nav: true,
        }
    }
});
// Project Slider




if($(window).width() <= 768){
    if(('.events-box-slider').length != 0){
        $('.events-box-slider').addClass('owl-carousel owl-theme');
        $('.events-box-slider').owlCarousel({
            loop:false,
            margin:15,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    nav:false,
                    dots: true,
                }, 
                413:{
                    
                    items:1,
                    nav:false,
                    dots: true,
                },
                574:{
                    
                    items:2,
                    nav:false,
                    dots: true,
                },
                767:{
                    items:2,
                }
                
            }
        });
    }
}

if($(window).width() <= 768){
    if(('.featured-box-area').length != 0){
        $('.featured-box-area').addClass('owl-carousel owl-theme');
        $('.featured-box-area').owlCarousel({
            loop:false,
            margin:15,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    nav:false,
                    dots: true,
                }, 
                480:{
                    
                    items:1,
                    nav:false,
                    dots: true,
                },
                574:{
                    
                    items:2,
                    nav:false,
                    dots: true,
                },
                767:{
                    items:3,
                }
                
            }
        });
    }
}



// Menu Toggle
$('#nav-icon3').click(function(){
    $(this).toggleClass('open');
});
// Menu Toggle

$(window).scroll(function(){
    var scrollheader  = $(window).scrollTop() > 50;
    
    if(scrollheader){
        $("header").addClass("header-sticky");
        // $("header").removeClass("bg-header");
    }
    else{
        $("header").removeClass("header-sticky");
        // $("header").addClass("bg-header");
    }
   
});

if($(window).width() >= 1199){
$(window).scroll(function(){
    var scrollheader  = $(window).scrollTop() > 500;
    
    if(scrollheader){
        $(".we-proud-area").addClass("we-proud-active");
        // $("header").removeClass("bg-header");
    }
    else{
        $(".we-proud-area").removeClass("we-proud-active");
        // $("header").addClass("bg-header");
    }
   
});
}



AOS.init();
