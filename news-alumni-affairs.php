<?php include 'include/header.php' ?>

<!-- Space -->
<div class="inner-page-header-seprator"></div>
<!-- Space -->


<section class="alumni-office-news-area">
    <div class="container">
        <!-- <h1 class="sec-heading text-center">
            <span>
                Alumni 
            </span>
            Office
        </h1> -->
        <div class="row">
            <div class="col-lg-6">
                <div class="alumni-office-news">
                    <a href="img/news/4.png" data-fancybox="galler">
                        <img src="img/news/4.png" class="img-fluid" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="alumni-office-news">
                    <a href="img/news/5.png" data-fancybox="galler">
                        <img src="img/news/5.png" class="img-fluid" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact-alumni-office" id="Contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="contact-content">
                    <h1 class="sec-heading"> <span>Contact</span> <br> Alumni Office</h1>
                    <ul class="contac-details">
                        <li>
                            <img src="img/loc.svg" alt="">
                            Office of Alumni Affairs, Habib University, Block 18, Gulistan-e-Jauhar – University Avenue, Off Shahrah-e-Faisal, Karachi – 75290, Sindh, Pakistan

                        </li>
                        <li>
                            <img src="img/em.svg" alt="">
                            alumni.affairs@habib.edu.pk
                        </li>
                        <li>
                            <img src="img/ph.svg" alt="">
                            +92 21 1110 42242 Ext. 4581
                        </li>
                        <li>
                            <img src="img/fb.svg" alt="">
                            facebook.com/groups/habibuniversityalumni
                        </li>
                        <li>
                            <img src="img/ti.svg" alt="">
                            9:00 am – 5:00 pm, Monday through Friday, 2nd Saturday of each month.
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="google-map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3618.7684150641808!2d67.13608751537706!3d24.905879849568592!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3eb33906ead1899d%3A0x3c0681e6f7d5dc14!2sHabib%20University!5e0!3m2!1sen!2s!4v1644827999335!5m2!1sen!2s" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'include/footer.php' ?>