<?php include 'include/header.php' ?>

<!-- Space -->
<div class="inner-page-header-seprator"></div>
<!-- Space -->

<section class="profile-area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="prof-inner">
                    <div class="profile-detail">
                        <img src="img/Shamez-Mukhi.jpg" class="img-fluid" alt="Shamez Mukhi">
                        <h5>Shamez Mukhi</h5>
                        <p>Director Student Life, Health & Wellness, Residential Life and Alumni Affairs</p>
                    </div>
                    <div class="prof-content">
                        <p>Shamez joined Habib University in August 2019. He has over 15 years of professional experience in Education and Youth Development. He has attained M.Sc. in Computer Science from University of Kent, UK and M.A. in Global Diplomacy from SOAS, University of London, UK. Shamez has worked extensively in students recruitment, career development, alumni programs and co-curricular activities including student-led clubs at the tertiary level. He also has vast experience in the design and implementation of youth development programs including arts, sports, youth camps and civic projects. He is a certified trainer and has conducted training sessions for over 8000 youth and volunteers in Pakistan.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'include/footer.php' ?>